/* 
 * opencv-image-roi - Example program for cvSetImageROI()
 *
 * Copyright (C) 2013  Antonio Ospite <ospite@studenti.unina.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <errno.h>
#include <opencv2/core/core_c.h>
#include <opencv2/highgui/highgui_c.h>

#define WIDTH 800
#define HEIGHT 480

int main(int argc, char *argv[])
{
	IplImage *image;
	CvRect viewport;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <image>\n", argv[0]);
		return -EINVAL;
	}

	image = cvLoadImage(argv[1], CV_LOAD_IMAGE_COLOR);
	if (!image) {
		fprintf(stderr, "Cannot load image\n");
		return -EINVAL;
	}

	cvNamedWindow("Viewport", 0);
	cvResizeWindow("Viewport", WIDTH, HEIGHT);

	viewport.x = 0;
	viewport.y = 0;
	viewport.width = WIDTH;
	viewport.height = HEIGHT;
	cvSetImageROI(image, viewport);

	cvShowImage("Viewport", image);

	cvWaitKey(0);

	cvDestroyWindow("Viewport");
	cvReleaseImage(&image);
	return 0;
}
